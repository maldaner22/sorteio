package com.example.thiagoweber.sorteio;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity {

    Button male;
    Button female;
    Button clean;
    Button send;
    TextView name;
    int random;
    String[] namesmale;
    String[] namesfemale;
    String randomName;

    EditText et;
    String yourName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        male = (Button) findViewById(R.id.male);
        female = (Button) findViewById(R.id.female);
        clean = (Button) findViewById(R.id.clean);
        send = (Button) findViewById(R.id.send);
        name = (TextView) findViewById(R.id.name);
        et = (EditText) findViewById(R.id.username);
        List<String> Test = Arrays.asList(getResources().getStringArray(R.array.namesmale));
        namesmale = getResources().getStringArray(R.array.namesmale);
        namesfemale = getResources().getStringArray(R.array.namesfemale);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,R.layout.spinner_layout,R.id.text_spinner,namesmale);
        spinner.setAdapter(adapter);

        Spinner spinner2 = (Spinner) findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter2=new ArrayAdapter<String>(this,R.layout.spinner_layout2,R.id.text_spinner2,namesfemale);
        spinner2.setAdapter(adapter2);
    }

    public void clickmale(View a) {
        randomize(a, namesmale);
        name.setTextColor(this.getResources().getColor(R.color.steel_blue));
    }

    public void clickfemale(View b) {
        randomize(b, namesfemale);
        name.setTextColor(this.getResources().getColor(R.color.deep_pink));
    }

    public void randomize(View c, String[] temp) {
        random = new Random().nextInt(temp.length);
        randomName = temp[random];
        String current = name.getText().toString();
        if (current == randomName) {
            randomize(c, temp);
        } else name.setText(randomName);
    }

    public void clickclean(View d) {
        name.setText(null);
    }

    public void clicksend (View e){
        yourName = et.getEditableText().toString();
        et.setText(null);
        name.setText(yourName);
    }
}